#include "queue.h"
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include "graph.h"
#include <cstdlib>
#include <sstream>
#include <vector>
#include <climits>
#include <iomanip>
#include <stack>
#include <set>

using namespace std;

#include "HashTable.h"


struct VertexType
{
	string name;
	bool marked;
	int distance;
	string previous;	
};

void insertionSort(string cities[], int numVertices);
void buildDGraph(Graph<string> &dGraph, string filename, string cities[], int &numVertices, int &numEdges, VertexType myVertices[]);
void printCities(Graph<string> dGraph, int numVertices, VertexType myVertices[], string cities[]);
string setStart(string start, int numVertices, VertexType myVertices[]);
void printRow(int location, VertexType myVertices[]);
int findMin(VertexType myVertices[], int numVertices);
void dijkstra(Graph<string> &dGraph, int numVertices, VertexType myVertices[]);
bool allMarked(int numVertices, VertexType myVertices[]);
void assignWeights(Graph<string> &dGraph, int numVertices, VertexType myVertices[], int startingLocation, Queue<string>& getTo);
//void alertUser(int numVertices, VertexType myVertices[]);
void alertUser(Graph<string> &dGraph, int numVertices, VertexType myVertices[], string & startV, string cities[]);
void printV(VertexType v);
bool cycle(Graph<string> &dGraph, int numVertices, string cyc[], VertexType myV[]);
bool isCyc(Graph<string> &dGraph, int numVertices, string cyc[], int pos, VertexType myV[]);
int pev;

int main(int argc, char* argv[])
{
	if(argc < 2)	// Checks to see if Filename is given
	{
		cout << "Incorrect Filename Given" << endl;
		return 0;
	}
	// Creation of Input File:
	string filename = argv[1];

	Graph<string> dGraph; 
	int numVertices;
	int numEdges;
	string startV;
	VertexType myVertices [50];
	string cities [50];
	buildDGraph(dGraph, filename, cities, numVertices, numEdges, myVertices);	// Builds the Graph using the data from the external file.
	alertUser(dGraph, numVertices, myVertices, startV, cities);
	printCities(dGraph, numVertices, myVertices, cities);	// Prints Cities found in file
	dijkstra(dGraph, numVertices, myVertices);	// Computes Dijkstra's Algorithm:u
	string cyc[50];
	if( cycle(dGraph, numVertices, cyc, myVertices))	//Detects if cycle exist or not
	{
		for(int i = 0; i < sizeof(cyc)/sizeof(cyc[0]); i++)
			cout << cyc[i] << " ";
	}
	return 1;
}

bool isCyc(Graph<string> &dGraph, int numVertices, string cyc[], int pos, VertexType myV[])
{
	dGraph.MarkVertex(myV[pos].name);
	cyc[pos] = myV[pos].name;
	for(int i = 0; i < numVertices; ++i)
	{
		if(!dGraph.IsMarked(myV[i].name) && isCyc(///<F3>STOPPED HERE///
	}
	return true;
}

bool cycle(Graph<string> &dGraph, int numVertices, string cyc[], VertexType myV[])
{
	dGraph.ClearMarks();
	for( int i = 0; i < numVertices; i++)
	{
		if(isCyc(dGraph, numVertices, cyc, i, myV))
		{
			return true;
		}
	}
	return false;
}

//Prints info of a single vertex
void printV(VertexType v)
{
	cout << v.name << "\t" << "\t" << v.distance
	<< "\t" << "\t" << v.previous << endl;
}

//Ask user which vertex they will begin with
void alertUser(Graph<string> &dGraph, int numVertices, VertexType myVertices[], string & startV, string cities[])
{
	cout << endl;
	cout << "--------------------------" << endl
	     << "-  Dijkstra's Algorithm  -" << endl
	     << "--------------------------" << endl;
	cout << "A graph was built for these " << 
		numVertices << " cities :" << endl;
}

// Computes Dijkstra's Algorithm:
void dijkstra(Graph<string> &dGraph, int numVertices, VertexType myVertices[])
{
	string startV;
	cout << "Please indicate your starting vertex : ";
	cin >> startV;
	bool state = true;
	int index;
	while(state)
	{
		for(int i = 0; i < numVertices; i++)
		{
			if(myVertices[i].name == startV)
			{
				index = i;
				state = false;
				i = numVertices;
				break;
			}
		}
		if(state == true)
		{
			cout << "That location does not exist." << endl
			<< "Please enter a new location : ";
			cin >> startV;
		}
	}

	//cout << index << endl;
	myVertices[index].distance = 0;
	myVertices[index].marked = true;
	pev = index;
	cout << "--------------------------" << endl;
	cout << "\t" << "Vertex" << "\t" << "\t" << "Distance"
	<< "\t" << "Previous" << endl << endl;
	//printV(myVertices[index]);
/*	string start;
	int index;
	setStart(start, numVertices, myVertices);
	for(int i = 0; i < numVertices; i++)
	{
		if(myVertices[i].name == start)
			index = i;
	}
*/	printRow(index, myVertices);
	Queue<string> flightQ;
	while(!allMarked(numVertices, myVertices))
	{	//cout << "assign";
		assignWeights(dGraph, numVertices, myVertices, index, flightQ);
		//cout << "findMin";
		index = findMin(myVertices, numVertices);
		pev = index;
		//cout << " pev = " << pev << endl;
		myVertices[index].marked = true;
		printRow(index, myVertices);
		//cout << index;

	}
	cout << "--------------------------" << endl << endl;

}

// Assign weights based on the starting location
void assignWeights(Graph<string> &dGraph, int numVertices, VertexType myVertices[], int startingLocation, Queue<string>& getTo)
{
	dGraph.GetToVertices(myVertices[startingLocation].name, getTo);
	//Queue <string> temp = getTo;
	while(!getTo.isEmpty())
	{
		for(int i = 0; i < numVertices; i++)
		{
			if(myVertices[i].name == getTo.getFront())
			{
				int d = dGraph.WeightIs(myVertices[startingLocation].name, myVertices[i].name);
				//cout << " " << myVertices[startingLocation].name << " " << myVertices[i].name << " " << d << endl;
				if( ((d+myVertices[pev].distance) < myVertices[i].distance) && (myVertices[i].marked == false) )
				{
					//cout << " ADD weight? pev = " << pev << endl;
					myVertices[i].previous = myVertices[startingLocation].name;
					myVertices[i].distance = d + myVertices[pev].distance;
					//cout << " New length = " << d << " + " << myVertices[pev].distance << " = " 
					//	<< myVertices[pev].distance + d << endl;
				}
				getTo.dequeue();
				i = numVertices;
			}
		}
	}
}

// Checks to see if all Vertices have been marked:
bool allMarked(int numVertices, VertexType myVertices[])
{
	for(int i = 0; i < numVertices; i++)
	{
		if(! myVertices[i].marked )
		{
			return false;
		}
	}
	return true;
}

// Builds the Graph using the data from the External File:
void buildDGraph(Graph<string> &dGraph, string filename, string cities[], int &numVertices, int &numEdges, VertexType myVertices[])
{
	char * temp [30] = {"ginfile1.dat"};
	cout << temp << endl
	<< temp[0] << endl;
	ifstream infile(temp[0]);

	const string NotFound = "ZZZ";
	HashTable<string> Cities(NotFound, 50);	

	string city1;
	numVertices = 0;
	numEdges = 0;

	VertexType myV = {"City", false, INT_MAX, "N/A"};

	while(getline(infile, city1, ';'))
	{
		//cout << city1 << "_";
		string city2;
		getline(infile, city2, ';');
		//cout << city2 << "_";
		string weight;
		getline(infile, weight);
		//cout << weight << endl;

		if((Cities.find(city1) == "ZZZ"))
		{
			dGraph.AddVertex(city1);
			Cities.insert(city1);
			cities[numVertices] = city1;
			myV.name = city1;
			myVertices[numVertices] = myV;
			numVertices++;
		}
		if((Cities.find(city2) == "ZZZ"))
		{
			dGraph.AddVertex(city2);
			Cities.insert(city2);
			cities[numVertices] = city2;
			myV.name = city2;
			myVertices[numVertices] = myV;
			numVertices++;
		}
		int w = 0;
		stringstream s(weight);
		s >> w;
		dGraph.AddEdge(city1, city2, w);
		numEdges++;
		
	}	
}

// Prints Cities found in External File:
void printCities(Graph<string> dGraph, int numVertices, VertexType myVertices[], string cities[])
{
	insertionSort(cities, numVertices);
	cout << endl;
	for(int i = 0; i < numVertices; i++)
	{
		cout << cities[i] << "\t" << "\t";
		
		if((i % 3) == 0 && i != 0)
			cout << endl;
	}
	cout << endl;
}

// Sorts the Cities Alphabetically:
void insertionSort(string cities[], int numVertices)
{
	int j;
	string key;
	for(int i = 1; i < numVertices; i++)
	{
		key = cities[i];
		j = i - 1;
		while(j >= 0 && cities[j] > key)
		{
			cities[j+1] = cities[j];
			j--;
		}
		cities[j+1] = key;
	}
}

//Sets the Starting Value in Dijsktra's Algorithm:
string setStart(string start, int numVertices, VertexType myVertices[])
{
	cout << " Please input your starting vertex: ";
	string response;
	getline(cin,response);
	bool isThere = false;
	for (int i = 0;	i < numVertices; i++)
	{
		if (myVertices[i].name.compare(response) == 0)
		{
			start = response;
			return start;
		}
	}
	while (!isThere)
	{
		cout << " Starting location does not exist..." << endl;
		cout << " Please input your new vertex: ";
        	string response;
        	cin >> response;
		for (int i = 0; i < numVertices; i++)
        	{
                	if (myVertices[i].name.compare(response) == 0)
			{
                        	start = response;
                        	return start;
                	}
        	}
	}
}

// Finds the minimum non-marked values in VertexType Array:
int findMin(VertexType myVertices[], int numVertices)
{
 		int minWeight = INT_MAX - 1;
                int minThing = -1;
		for (int i = 0; i < numVertices; i++)
                {
                        if (myVertices[i].distance < minWeight && !myVertices[i].marked)
                        {
                                minWeight = myVertices[i].distance;
                                minThing = i;
                        }
			else if (myVertices[i].distance == minWeight && !myVertices[i].marked)
        		{
                                minWeight = myVertices[i].distance;
                                minThing = i;
                        }

		}        
		return minThing;
}

// Prints a given row in a Summary Table:
void printRow(int location, VertexType myVertices[])
{
	if (myVertices[location].distance != -1)
	cout << setw(14) << myVertices[location].name << setw(18) << myVertices[location].distance << setw(16) << myVertices[location].previous << endl;
	else
	 cout << setw(14) << myVertices[location].name << setw(18) << "Not On Path" << setw(16) << myVertices[location].previous << endl;
}

